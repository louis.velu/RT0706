#!/bin/bash
## permet de croiser une db avec une combo-list pour essayer à définir le type salage
while IFS= read -r line; do
	mail=$(echo $line | cut -d ":" -f 2)
	mail=$(echo $mail | tr '[:upper:]' '[:lower:]')

	hash=$(echo $line | cut -d ":" -f 4)
	salt=$(echo $line | cut -d ":" -f 5)
	#echo $hash
	#echo $salt

	char1=${mail:0:1}
	char2=${mail:1:1}
	grp=$(grep -h -a  -d recurse $mail ~/BreachCompilation/data/$char1/$char2)
	if [[ -n $grp ]];
	then
		echo $grp | tr ' ' '\n' | cut -d ':' -f 2 >> ~/dico/try-password
		echo $hash:$salt >> ~/hashcat/input_cross_db
	fi

done < ~/BreachCompilation/db/Tunngle_RF/Data/tunngle.net.txt

