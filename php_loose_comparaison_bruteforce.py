import hashlib
chars=["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9"]
seed=[-1]
while True:
	seed[0]+=1
	for i in range(0,len(seed)):
		if seed[i] >= len(chars):
			seed[i]=0
			if i == len(seed)-1:
				seed.append(0)
				print(str(i+1))
			else:
				seed[i+1]+=1
	tohash=""
	for i in range(0,len(seed)):
		tohash+=chars[seed[i]]
	hash=hashlib.md5(tohash.encode()).hexdigest()
	if hash[0] == "0" and hash[1] == "e":
		ok = True
		for e in range(2, len(hash)):
			if hash[e] not in ["0","1","2","3","4","5","6","7","8","9"]:
				ok = False
		if ok:
			print(str(seed)+" // "+hash)
