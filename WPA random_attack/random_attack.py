#!/usr/bin/env python3
#louis vélu

import secrets
import os
import time

#print(secrets.token_hex(24))

KEY_LENGTH = 26
DICO_SIZE = 1000000
INPUT_HASH = "../hashcat_input.hccapx"
DICO_FILE = "generated_dico.txt"
OUTPUT_RESULT = "result_hashcat.txt"

while True:
	dico = {}
	if os.path.exists(DICO_FILE):
		os.remove(DICO_FILE)
	file_dico = open(DICO_FILE, "w+")
	for e in range(0, DICO_SIZE):
		tmp = secrets.token_hex(KEY_LENGTH)[:KEY_LENGTH]+"\n"
		file_dico.write(tmp.upper())
	file_dico.close()
	print("start hashcat")
	t0 = time.time()
	os.system("hashcat -m 2500 -a 0 --quiet -o "+OUTPUT_RESULT +" "+INPUT_HASH+" "+DICO_FILE+" 2>/dev/null")
	#os.system("hashcat -m 2500 -a 0 -o "+OUTPUT_RESULT+" "+INPUT_HASH+" "+DICO_FILE)
	t1 = time.time()
	#print(t1-t0)
exit()
