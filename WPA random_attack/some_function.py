import secrets
import os
import re

####### Paramètres #######
# intervale : ["a","b"]
intervale_list = []

char_list = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
INPUT_HASH = "../hashcat_input.hccapx"
DICO_FILE = "generated_dico.txt"
OUTPUT_RESULT = "result_hashcat.txt"
KEY_LENGTH = 26
MAX_REP = 3
DIFFICULTY = 3

####### Functions #######

## fusionne les intervales de la list
def fusion_intervale_list():
    global intervale_list
    new_intervale_list = [] # le nouvelle liste
    for intervale_1 in intervale_list : #pour chaque intervale
        try :
            for intervale_2 in new_intervale_list :
                if (intervale_1[0] <= intervale_2[1] and intervale_1[1] > intervale_2[0]): #intervale_1 = [b,c] et intervale_2 = [a,b] => Fusion [a,c]
                    new_intervale_list.remove(intervale_2) #on retir l'intervale 2 de la nouvelle liste
                    intervale_1[0] = intervale_2[1]
                    continue
                else if(intervale_1[1] >= intervale_2[0] and intervale_1[0] < intervale_2[1]): #intervale_1 = [a,b] et intervale_2 = [b,c] => Fusion [a,c]
                    new_intervale_list.remove(intervale_2) #on retir l'intervale 2 de la nouvelle liste
                    intervale_1[1] = intervale_2[0]
                    continue
                else if (intervale_1[0] >= intervale_2[0] and intervale_1[1] <= intervale_2[1]) : # intervale_1 = [b,c] et intervale_2 = [a,d] /!\ intervale_1 compris dans intervale_2 => erreur, on supprime l'intervale_1
                    raise "intervale inclu"
            new_intervale_list.append(intervale_1) #on ajoute l'intervale_1 à la liste
        except "intervale inclu":
            continue #valeur supprimé de la nouvelle liste
    intervale_list = new_intervale_list #on met à jour la liste

def ajouter_intervale(intervale): #ajoute un intervale dans le tableau
    global intervale_list
    intervale_list.append(intervale)
    fusion_intervale_list()

def count_intervales(): #retourne le nombre d'intervale
    return len(intervale_list)

def next_non_random(): #retourne la valeur de fin du première intervale
    return intervale_list[0][1]

def search_intervale (intervale): #verifie si l'intervale est déjà présent dans le tableau
    for intervale_1 in intervale_list :
        if (intervale[0] >= intervale_1[0] and intervale[1] <= intervale_1[1]) : # l'intervale est égale ou inclu à un autre : return false
            return False
    return True
    
def ajuste_intervale(intervale): #ajuste l'intervale si une partie à déjà été couvert
    for intervale_1 in intervale_list :
        if (intervale[0] > intervale_1[0] and intervale[0] < intervale_1[1]):  #intervale = [b,d] et intervale_1 = [a,c] => Fusion [c,d]
            intervale[0] = intervale_1[1]
        else if (intervale[1] > intervale_1[0] and intervale[1] < intervale_1[1]) : #intervale = [a,c] et intervale_1 = [b,d] => Fusion [a,b]
            intervale[1] = intervale_1[0]
    return intervale
    
def rand_hex_key(length): # retourne un clé random hexadécimal de taille length
    return (secrets.token_hex(KEY_LENGTH)+"").upper()


def inc_key(key): #incrémente la clé de 1
    carry = True #si chaine vide, on return "1"
    for e in range(len(key)):
        carry = False
        for i in range(len(char_list)):
            if key[e] == char_list[i]:
                if i == len(char_list)-1 :
                    key[e] = char_list[0]
                    carry = True
                else :
                    key[e] = char_list[i+1]
                    break
    if carry :
        "1"+key[e]
    return key

def exclude_repeated_key(n,length): #note les intervale de toutes les clés qui contiennent un caractère répété plus de n foix de suite
    #pour chaque taille d'espace laissé en début de clé avant d'avoir la suite de 3 caractère
    for e in range(length-n):
        #on génère une base qu'on va incrémenter à chaque tour
        base=""
        for z in range(e):
            base+=char_list[0]
        
        #tant qu'on a pas couvert toutes les valeurs de la base
        while len(base) == e:
        
            # pour chaque caractère
            for i in range(len(char_list)):
                #génère la suite de caractère
                suite=""
                for z in range(n):
                    suite+=char_list[i]
            
            #on créer l'intervalle associer à cette valeur :
            
            intervale_debut = base+suite
            intervale_fin = base+suite
            
            for i in range(length-n-e) :
                intervale_debut += char_list[0]
                intervale_fin += char_list[len(char_list)-1]
            intervale_fin = inc_key(intervale_fin)
            
            #on ajoute l'intervale à la liste
            ajouter_intervale([intervale_debut,intervale_fin])
            
            base = inc_key(base)



def create_dico (base, length, n=1) :  #génère un dictionnaire avec une base, la taille de la clé et le nombre max de même caractère à la suite, retourne un intervalle de valeur
    #supprime le dictionnaire si il existe
    if os.path.exists(DICO_FILE):
        os.remove(DICO_FILE)
    dico = open(DICO_FILE, "w+")
    
    #créer le début et la fin de l'intervalle
    intervale_debut = base
    intervale_fin = base
            
    for i in range(length-len(base)) :
        intervale_debut += char_list[0]
        intervale_fin += char_list[len(char_list)-1]
    intervale_fin = inc_key(intervale_fin)
    
    #génère le paterne de recherche de répétition de caractère :
    patern = "(.)"
    for i in range(n-1) :
        patern+="\1"
    
    #génère le dico
    actual = intervale_debut
    while actual != intervale_fin :
        #si pas 
        if not re.match(patern, actual)
            dico.write(actual)
        #incrémente la clé
        actual = inc_key(actual)
    
    dico.close()
    return [intervale_debut,intervale_fin]



####### Main #######

exclude_repeated_key(MAX_REP,KEY_LENGTH)

while True:
    base = rand_hex_key(KEY_LENGTH-DIFFICULTY)
    
    first_value = base
    for e in range(DIFFICULTY):
        first_value += char_list[0]
    
    interval = []
    if search_intervale([first_value,first_value]) :
        interval = create_dico(base, KEY_LENGTH, MAX_REP)
    
    os.system("hashcat -m 2500 -a 0 --quiet -o "+OUTPUT_RESULT +" "+INPUT_HASH+" "+DICO_FILE+" 2>/dev/null")
    
    ajouter_intervale(interval)